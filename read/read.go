package main

import (
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis"
	_ "github.com/joho/godotenv/autoload"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var Db *gorm.DB
var mahasiswaCache *redis.Client

type Mahasiswa struct {
	NPM  string `gorm:"primaryKey" json:"npm"`
	Nama string `json:"nama"`
}

type Response struct {
	Status string `json:"status"`
	NPM    string `json:"npm"`
	Nama   string `json:"nama"`
}

func getFromDb(npm string) Mahasiswa {
	var mahasiswa Mahasiswa
	Db.Table("mahasiswas").Where("npm = ?", npm).Scan(&mahasiswa)

	return mahasiswa
}

func getFromCache(npm string, trxId string) Mahasiswa {
	var mahasiswa Mahasiswa
	identifier := npm + trxId
	cachedMahasiswaData, err := mahasiswaCache.Get(identifier).Result()
	if err == redis.Nil {
		mahasiswa = getFromDb(npm)
		mahasiswaCache.Set(identifier, mahasiswa.Nama, time.Second*time.Duration(300))
	} else if err == nil {
		mahasiswa = Mahasiswa{
			NPM:  npm,
			Nama: cachedMahasiswaData,
		}
	} else {
		log.Fatalln("Error connecting to cache")
	}

	return mahasiswa
}

func getData(c *gin.Context) {
	npm := c.Param("npm")

	trxId := c.Param("trxId")
	trxId = strings.TrimPrefix(trxId, "/")

	var mahasiswa Mahasiswa

	if trxId == "" {
		mahasiswa = getFromDb(npm)
	} else {
		mahasiswa = getFromCache(npm, trxId)
	}

	response := Response{
		Status: "OK",
		NPM:    npm,
		Nama:   mahasiswa.Nama,
	}

	c.JSON(http.StatusOK, response)
}

func main() {
	dsn := os.Getenv("DSN")
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatalln("Can't connect to DB")
	}
	Db = db
	db.AutoMigrate(&Mahasiswa{})

	redisAddr := os.Getenv("REDIS_HOST") + ":" + os.Getenv("REDIS_PORT")
	redisPass := os.Getenv("REDIS_PASSWORD")

	mahasiswaCache = redis.NewClient(&redis.Options{
		Addr:     redisAddr,
		Password: redisPass,
		DB:       0,
	})

	r := gin.Default()

	r.GET("/:npm/*trxId", getData)
	r.GET("/:npm", getData)

	port := ":" + os.Getenv("PORT")
	r.Run(port)
}
