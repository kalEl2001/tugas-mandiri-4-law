from locust import HttpUser, task, between


class TM4User(HttpUser):
    wait_time = between(1, 5)

    # # Non-idempotent
    # @task
    # def get_mahasiswa(self):
    #     self.client.get('/read/111/')

    # Idempotent
    @task
    def get_mahasiswa_idempotent(self):
        self.client.get('/read/111/123')
