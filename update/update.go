package main

import (
	"log"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	_ "github.com/joho/godotenv/autoload"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var Db *gorm.DB

type Mahasiswa struct {
	NPM  string `gorm:"primaryKey" json:"npm"`
	Nama string `json:"nama"`
}

func createOrUpdate(c *gin.Context) {
	var req Mahasiswa
	if err := c.Bind(&req); err != nil || req.NPM == "" || req.Nama == "" {
		c.JSON(400, gin.H{"error": err.Error()})
		return
	}

	if Db.Model(&req).Where("npm = ?", req.NPM).Updates(&req).RowsAffected == 0 {
		Db.Create(&req)
	}

	c.JSON(http.StatusOK, gin.H{
		"status": "OK",
	})
}

func main() {
	dsn := os.Getenv("DSN")
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatalln("Can't connect to DB")
	}
	Db = db
	db.AutoMigrate(&Mahasiswa{})

	r := gin.Default()

	r.POST("/", createOrUpdate)

	port := ":" + os.Getenv("PORT")
	r.Run(port)
}
